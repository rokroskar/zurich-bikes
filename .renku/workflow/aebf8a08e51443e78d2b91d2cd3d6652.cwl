class: Workflow
cwlVersion: v1.0
hints: []
inputs:
  input_1:
    default:
      class: File
      path: ../../src/plot_data.py
    streamable: false
    type: File
  input_2:
    default:
      class: File
      path: ../../data/preprocessed/zhbikes.parquet
    streamable: false
    type: File
outputs:
  output_0:
    outputSource: step_1/output_0
    streamable: false
    type: File
  output_1:
    outputSource: step_1/output_1
    streamable: false
    type: File
requirements: []
steps:
  step_1:
    in:
      input_1: input_1
      input_2: input_2
    out:
    - output_0
    - output_1
    run: eee5ee95e6cf46a4ba8ca5cc65da0d75_python.cwl
