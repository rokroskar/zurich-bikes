arguments: []
baseCommand:
- python
class: CommandLineTool
cwlVersion: v1.0
hints: []
inputs:
  input_1:
    default:
      class: File
      path: ../../src/plot_data.py
    inputBinding:
      position: 1
      separate: true
      shellQuote: true
    streamable: false
    type: File
  input_2:
    default:
      class: File
      path: ../../data/preprocessed/zhbikes.parquet
    inputBinding:
      position: 2
      separate: true
      shellQuote: true
    streamable: false
    type: File
outputs:
  output_0:
    outputBinding:
      glob: figs/cumulative.png
    streamable: false
    type: File
  output_1:
    outputBinding:
      glob: figs/grid_plot.png
    streamable: false
    type: File
permanentFailCodes: []
requirements:
- class: InlineJavascriptRequirement
- class: InitialWorkDirRequirement
  listing:
  - entry: '$({"listing": [], "class": "Directory"})'
    entryname: figs
    writable: true
successCodes: []
temporaryFailCodes: []
