class: Workflow
cwlVersion: v1.0
hints: []
inputs:
  input_1:
    default:
      class: File
      path: ../../src/clean_data.py
    streamable: false
    type: File
  input_2:
    default:
      class: Directory
      listing: []
      path: ../../data/zhbikes
    streamable: false
    type: Directory
  input_3:
    default: data/preprocessed/zhbikes.parquet
    streamable: false
    type: string
outputs:
  output_0:
    outputSource: step_1/output_0
    streamable: false
    type: File
requirements: []
steps:
  step_1:
    in:
      input_1: input_1
      input_2: input_2
      input_3: input_3
    out:
    - output_0
    run: 17eddc08def14a2ca96b0faabd73b1b4_python.cwl
